import React, { Component } from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import * as actions from "../../store/actions";
import "./Login.scss";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "ads",
      password: "423",
      isShowPassword: false,
    };
  }
  handleOnChangeUsername = (e) => {
    this.setState({
      username: e.target.value,
    });
  };
  handleOnChangePassword = (e) => {
    this.setState({
      password: e.target.value,
    });
  };
  handleLogin = (e) => {
    console.log("username: " + this.state.username);
    console.log("password: " + this.state.password);
  };
  handleShowHidePassword = () => {
    this.setState({
      isShowPassword:!this.state.isShowPassword
    });
  };
  render() {
    return (
      <div className="login-background">
        <div className="login-container">
          <div className="login-content">
            <div className="login-title">Login</div>
            <div className="col-12 form-group">
              <label>Username:</label>
              <input
                type="text"
                className="form-control"
                value={this.state.username}
                onChange={(e) => {
                  this.handleOnChangeUsername(e);
                }}
              ></input>
            </div>
            <div className="col-12 form-group">
              <label>Password:</label>
              <div className="custom-input-password">
                <input
                  type={this.state.isShowPassword ? "text" : "password"}
                  className="form-control"
                  placeholder="Enter your password"
                  value={this.state.password}
                  onChange={(e) => {
                    this.handleOnChangePassword(e);
                  }}
                ></input>
                <span
                  onClick={() => {
                    this.handleShowHidePassword();
                  }}
                >
                  <i
                    class={
                      this.state.isShowPassword ? "fa fa-eye" : "fa fa-eye-slash"
                    }
                    aria-hidden="true"
                  ></i>
                </span>
              </div>
            </div>
            <div className="col-12 text-center">
              <button
                className="btn-login"
                onClick={(e) => {
                  this.handleLogin();
                }}
              >
                Login
              </button>
            </div>
            <div className="col-12 form-footer">
              <span className="forgot-password">Forgot password?</span>
            </div>
            <div className="col-12">
              <span className="text-other-login form-footer">
                or Login with
              </span>
            </div>
            <div className="col-12 social-login form-footer">
              <i className="fab fa-google-plus-g google"></i>
              <i className="fab fa-facebook-f facebook"></i>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.app.language,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    navigate: (path) => dispatch(push(path)),
    adminLoginSuccess: (adminInfo) =>
      dispatch(actions.adminLoginSuccess(adminInfo)),
    adminLoginFail: () => dispatch(actions.adminLoginFail()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
