const { Sequelize } = require("sequelize");

// Option 3: Passing parameters separately (other dialects)
const sequelize = new Sequelize("database_cuong", "root", "", {
  host: "localhost",
  dialect: "mysql",
  logging: false, //bỏ notification query data. Example: select * from User
});

let connectDB = async () => {
  try {
    //To check already connect success or not!
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};

module.exports = connectDB;
