"use strict";
module.exports = {
  // add data into database.
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("doctor_clinic_specialty", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      doctorId: {
        type: Sequelize.INTEGER,
      },
      clinicId: {
        type: Sequelize.INTEGER,
      },
      specialtyId: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  //DOWN: rollback version //
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("doctor_clinic_specialty");
  },
};
