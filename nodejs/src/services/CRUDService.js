import bcryptjs from "bcryptjs";
import db from "../models/index";

const salt = bcryptjs.genSaltSync(10);

let createNewUser = async (data) => {
  try {
    const hashPasswordFromBcryptjs = await hashUserPassword(data.password);
    await db.User.create({
      email: data.email,
      password: hashPasswordFromBcryptjs,
      firstName: data.firstName,
      lastName: data.lastName,
      address: data.address,
      phoneNumber: data.phoneNumber,
      gender: data.gender === "1" ? true : false,
      roleId: data.roleId,
    });
    return "OK create Use Success";
  } catch (e) {
    console.log(e);
  }
};

let hashUserPassword = (password) => {
  try {
    const hashPassword = bcryptjs.hashSync(password, salt);
    return hashPassword;
  } catch (e) {
    console.log(e);
  }
};

let getAllUser = async () => {
  let users = await db.User.findAll({
    raw: true,
  });
  return users;
};
let getUserInfoById = async (userId) => {
  try {
    const user = await db.User.findOne({
      where: { id: userId },
      raw: true,
    });
    if (user) {
      return user;
    } else {
      return {};
    }
  } catch (error) {
    console.log(error);
  }
};
let updateUserData = async (data) => {
  try {
    let user = await db.User.findOne({
      where: {
        id: data.id,
      },
    });
    if (user) {
      user.firstName = data.firstName;
      user.lastName = data.lastName;
      user.address = data.address;

      await user.save();
      let allUsers = await db.User.findAll();
      return allUsers;
    } else {
      return;
    }
  } catch (error) {
    console.log(error);
  }
};

let deleteUserById = async (userId) => {
  try {
    let user = await db.User.findOne({
      where: { id: userId },
    });
    if (user) {
      await user.destroy();
      return;
    }
  } catch (error) {
    console.log(error);
  }
};
module.exports = {
  createNewUser: createNewUser,
  getAllUser: getAllUser,
  getUserInfoById: getUserInfoById,
  updateUserData: updateUserData,
  deleteUserById: deleteUserById,
};
