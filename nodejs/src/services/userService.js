import db from "../models/index";
import bcrypt from "bcryptjs";

const handleUserLogin = async (email, password) => {
  try {
    let userData = {};
    const isExist = await checkUserEmail(email);
    if (isExist) {
      let user = await db.User.findOne({
        attributes: ["email", "roleId", "password"],
        where: { email: email },
        raw: true,
      });
      if (user) {
        let check = await bcrypt.compareSync(password, user.password); // false
        if (check) {
          userData.errCode = 0;
          userData.errMessage = "OK";
          delete user.password;
          userData.user = user;
        } else {
          userData.errCode = 3;
          userData.errMessage = "Wrong password";
        }
      } else {
        userData.errCode = 2;
        userData.errMessage = `User's not found !`;
      }
    } else {
      userData.errCode = 1;
      userData.errMessage =
        "Your email isn't exist in your system. Plz try other email!";
    }
    return userData;
  } catch (error) {
    return error;
  }
};
const checkUserEmail = (userEmail) => {
  return new Promise(async (resolve, reject) => {
    try {
      let user = await db.User.findOne({
        where: { email: userEmail },
      });
      if (user) {
        resolve(true);
      } else {
        resolve(false);
      }
    } catch (error) {
      reject(error);
    }
  });
};

let compareUserPassword = async () => {
  try {
  } catch (error) {
    return;
  }
};

module.exports = {
  handleUserLogin: handleUserLogin,
};
